#!/usr/bin/python3

import lzma
import re
import subprocess
import sys


verbose = False


def log(msg):
    if verbose:
        sys.stderr.write(f"{msg}\n")
        sys.stderr.flush()


def parse(para):
    name = None
    version = None
    for line in para.splitlines():
        if line.startswith("Package:"):
            name = line.split()[1]
        elif line.startswith("Version:"):
            version = line.split()[1]
    return name, version


def crate_name(name):
    if not name.startswith("librust-"):
        return None
    name = strip_prefix(name, "librust-")
    name = strip_suffix(name, "-dev")
    return name


def strip_prefix(s, prefix):
    if s.startswith(prefix):
        return s[len(prefix) :]
    else:
        return s


def strip_suffix(s, suffix):
    if s.endswith(suffix):
        return s[: -len(suffix)]
    else:
        return s


def crate_version(version):
    if "+really" in version:
        version = version.split("really")[-1]
        version = strip_prefix(version, ".")
    version = "-".join(version.split("~"))
    parts = version.split("-")
    if len(parts) == 1:
        return parts[0]
    else:
        return "-".join(parts[:-1])


output = subprocess.run(
    [
        "curl",
        "-s",
        "http://deb.debian.org/debian/dists/unstable/main/binary-amd64/Packages.xz",
    ],
    check=True,
    capture_output=True,
)

packages = lzma.decompress(output.stdout).decode("UTF8")
paras = packages.split("\n\n")

for para in paras:
    (name, version) = parse(para)
    if name is not None and version is not None:
        crate = crate_name(name)
        v = crate_version(version)
        log(f"Debian package {name} -> {crate}, {version} -> {v}")
        if crate is not None:
            print(crate, v)
