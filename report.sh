#!/bin/bash

set -euo pipefail

sq_dir="$1"

./debian-crate-packages >crates-in-debian
cargo run -q -- --style=markdown --features crypto-nettle crates-in-debian "$sq_dir" |
	pandoc -H style.css -f markdown /dev/stdin -o problems.html --metadata title="Problems packaging sq for Debian"
